# Praktikum Sisop Modul 4 (BJ-U03)
Group Members:
1. Ahmad Danindra Nugroho (5025211259)
2. Mardhatillah Shevy Ananti (5025211070)
3. Raden Roro Kayla Angelica Priambudi (5025211262)

## Question 1  
Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.
**Catatan: **
- Cantumkan file hubdocker.txt yang berisi URL Docker Hub kalian (public).
- Perhatikan port  pada masing-masing instance.

#### 1A    
**_Question :_**    
Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

kaggle datasets download -d bryanb/fifa-player-stats-database

Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 

**_Aplication On Program:_**    

```c
int main() {
    int status;
    pid_t child_id;

    child_id = fork();

    if (child_id < 0) {
        printf("Failed to create a new process.\n");
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        // This is the child process
        char *downloadCommand[] = {"kaggle", "datasets", "download", "-d", "bryanb/fifa-player-stats-database", NULL};
        execvp("kaggle", downloadCommand);
        exit(EXIT_FAILURE);
    }

    waitpid(child_id, &status, 0);

    if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
        child_id = fork();

        if (child_id < 0) {
            printf("Failed to create a new process.\n");
            exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
            // This is the child process
            char *extractCommand[] = {"unzip", "fifa-player-stats-database.zip", NULL};
            execvp("unzip", extractCommand);
            exit(EXIT_FAILURE);
        }

        waitpid(child_id, &status, 0);

```
**Explanation:**
1. The program begins by declaring the status variable to store the exit status of the child process and the child_id variable to store the process ID of the child process.

2.The fork() system call is used to create a new process. The return value of fork() is assigned to child_id.

3. If child_id is less than 0, it means that the creation of the child process failed. In such a case, an error message is printed, and the program exits with a failure status (EXIT_FAILURE).

4. If child_id is equal to 0, it means that the current process is the child process. Inside this condition, the code sets up a command to download the dataset using the Kaggle CLI.

5. char *downloadCommand[] = { ... };: This line initializes an array of strings downloadCommand that represents the command and its arguments. The command is "kaggle datasets download -d bryanb/fifa-player-stats-database", which instructs Kaggle CLI to download the dataset with the given ID.

6. execvp("kaggle", downloadCommand);: The execvp() system call replaces the current process with a new process specified by the given command and arguments. In this case, it executes the "kaggle" command with the provided arguments to download the dataset. If the execvp() call is successful, the subsequent lines will not be executed in the child process.

7. exit(EXIT_FAILURE);: If the execvp() call fails, the child process exits with a failure status.

8. After the child process has finished, the parent process executes the following line:

9. waitpid(child_id, &status, 0);: The waitpid() system call is used to wait for the child process with the specified process ID (child_id) to terminate. The status variable will hold the exit status of the child process.
If the child process exited normally (i.e., without errors) and the exit status is 0, the parent process proceeds to the next section.

10. The parent process creates another child process by calling fork() again.

11. Similar to the previous child process, this new child process is responsible for extracting the downloaded dataset.

12. char *extractCommand[] = { ... };: This line initializes an array of strings extractCommand that represents the command and its arguments. The command is "unzip fifa-player-stats-database.zip", which extracts the downloaded dataset.

13. execvp("unzip", extractCommand);: The execvp() system call replaces the current process with a new process specified by the given command and arguments. In this case, it executes the "unzip" command with the provided arguments to extract the dataset. If the execvp() call is successful, the subsequent lines will not be executed in the child process.

14. exit(EXIT_FAILURE);: If the execvp() call fails, the child process exits with a failure status.

15. The parent process waits for the second child process to finish using waitpid().

16. After the second child process has finished, the parent process completes its execution, and the program terminates.

#### 1B   
**_Question :_**    
Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.

**_Aplication On Program_**     
```c
 system("awk -F\",\" '{if ($3 < 25 && $8 > 85 && $9 != \"Manchester City\") print \"Name :\",$2,\"\\n Club: \",$9,\"\\n Age: \",$3,\"\\n Potential: \",$8,\"\\n Photo: \",$4,\"\\n Nationality: \",$5,\"\\n\"}' FIFA23_official_data.csv");

```
**Explanation:**
1. The command is invoking the awk utility to process the file FIFA23_official_data.csv and extract specific information based on certain conditions. Here's a breakdown of the command:

2. awk: It is a versatile text processing tool that operates on structured text data.

3. -F\",\": This option sets the field separator to , (comma). The field separator is used to split each line into separate fields based on the specified delimiter.

4. '{if ($3 < 25 && $8 > 85 && $9 != \"Manchester City\") print \"Name :\",$2,\"\\n Club: \",$9,\"\\n Age: \",$3,\"\\n Potential: \",$8,\"\\n Photo: \",$4,\"\\n Nationality: \",$5,\"\\n\"}': This is the AWK script enclosed within single quotes. It defines the processing logic to be applied to each line of the input file.

5. if ($3 < 25 && $8 > 85 && $9 != \"Manchester City\"): This condition checks if the value in the third field (age) is less than 25, the value in the eighth field (potential) is greater than 85, and the value in the ninth field (club) is not equal to "Manchester City".

6. print \"Name :\",$2,\"\\n Club: \",$9,\"\\n Age: \",$3,\"\\n Potential: \",$8,\"\\n Photo: \",$4,\"\\n Nationality: \",$5,\"\\n\": If the condition is true, this part specifies the content to be printed. It includes the values of various fields, such as name, club, age, potential, photo, and nationality. The \\n represents a newline character.

7. FIFA23_official_data.csv: This is the input file that will be processed by AWK. It is assumed to be in the current working directory.

#### 1C
**_Question :_**    
Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.

**_Aplication On Program:_**      
```dockerfile
# Use base image
FROM ubuntu:20.04

# Install any required dependencies or packages
RUN apt-get update && \
    apt-get install -y gcc unzip python3-pip && \
    pip install kaggle

# Download the Kaggle API credentials file (kaggle.json) and move it to the appropriate location
COPY kaggle.json /root/.kaggle/kaggle.json

# Set the permissions for the Kaggle API credentials file
RUN chmod 600 /root/.kaggle/kaggle.json

# Install the unzip package
RUN apt-get install -y unzip

# Copy files into the container
COPY storage.c /app/storage.c

# Set working directory
WORKDIR /app

# Compile the storage.c file
RUN gcc -o storage storage.c

# Set an entrypoint script to execute the storage program and display the output
CMD ["./storage"]

```

#### 1D
**_Question :_**    
Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.

**_Aplication:_**    
![Screen_Shot_2023-06-03_at_18.43.04](/uploads/3cac6042898f278fe9a2ff2c9ef4e272/Screen_Shot_2023-06-03_at_18.43.04.png)
![Screen_Shot_2023-06-03_at_18.43.24](/uploads/593c292e396a28e44bbd7e14a056c3ef/Screen_Shot_2023-06-03_at_18.43.24.png)

#### 1E
**_Question :_**    
Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.

**_Aplication On Program:_**      
```dockerfile
version: '3'
services:
#instance 1
  1.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8080:80"
#instance 2
  2.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8081:80"
  #instance 3
  3.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8082:80"
  #instance 4
  4.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8083:80"
  #instance 5
  5.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8084:80"


```

#### Source Code Question 1
**storage.c:**
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main() {
    int status;
    pid_t child_id;

    child_id = fork();

    if (child_id < 0) {
        printf("Failed to create a new process.\n");
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        // This is the child process
        char *downloadCommand[] = {"kaggle", "datasets", "download", "-d", "bryanb/fifa-player-stats-database", NULL};
        execvp("kaggle", downloadCommand);
        exit(EXIT_FAILURE);
    }

    waitpid(child_id, &status, 0);

    if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
        child_id = fork();

        if (child_id < 0) {
            printf("Failed to create a new process.\n");
            exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
            // This is the child process
            char *extractCommand[] = {"unzip", "fifa-player-stats-database.zip", NULL};
            execvp("unzip", extractCommand);
            exit(EXIT_FAILURE);
        }

        waitpid(child_id, &status, 0);

        system("awk -F\",\" '{if ($3 < 25 && $8 > 85 && $9 != \"Manchester City\") print \"Name :\",$2,\"\\n Club: \",$9,\"\\n Age: \",$3,\"\\n Potential: \",$8,\"\\n Photo: \",$4,\"\\n Nationality: \",$5,\"\\n\"}' FIFA23_official_data.csv");
    }

    return 0;
}
```

**dockerfile:**
```dockerfile
# Use base image
FROM ubuntu:20.04

# Install any required dependencies or packages
RUN apt-get update && \
    apt-get install -y gcc unzip python3-pip && \
    pip install kaggle

# Download the Kaggle API credentials file (kaggle.json) and move it to the appropriate location
COPY kaggle.json /root/.kaggle/kaggle.json

# Set the permissions for the Kaggle API credentials file
RUN chmod 600 /root/.kaggle/kaggle.json

# Install the unzip package
RUN apt-get install -y unzip

# Copy files into the container
COPY storage.c /app/storage.c

# Set working directory
WORKDIR /app

# Compile the storage.c file
RUN gcc -o storage storage.c

# Set an entrypoint script to execute the storage program and display the output
CMD ["./storage"]

```
**docker-compose.yml:**
```docker-compose.yml
version: '3'
services:
#instance 1
  1.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8080:80"
#instance 2
  2.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8081:80"
  #instance 3
  3.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8082:80"
  #instance 4
  4.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8083:80"
  #instance 5
  5.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8084:80"
```    
    
## Question 3 
Dhafin adalah seorang yang sangat pemalu, tetapi saat ini dia sedang mengagumi seseorang yang dia tidak ingin seorangpun tahu. Maka dari itu, dia melakukan berbagai hal sebagai berikut.

### 3A
Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah /etc yang bernama secretadmirer.c.

#### Source Code
```
#define FUSE_USE_VERSION 28

#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <limits.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

static const char *dirpath = "/home/mardha/inifolderetc/sisop";

static const char *password = "12345";

static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void encodeBase64File(const char *filename) {
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        return;
    }

    // Get file size
    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    // Allocate memory for file content
    uint8_t *file_content = (uint8_t *)malloc(file_size);
    if (file_content == NULL) {
        printf("Memory allocation failed\n");
        fclose(file);
        return;
    }

    // Read file content
    size_t bytes_read = fread(file_content, 1, file_size, file);
    fclose(file);

    if (bytes_read != file_size) {
        printf("Failed to read file: %s\n", filename);
        free(file_content);
        return;
    }

    // Encode file content as Base64
    uint8_t input[3], output[4];
    size_t input_len = 0;
    size_t output_len = 0;
    size_t i = 0;
    
    file = fopen(filename, "wb");
    if (file == NULL) {
        printf("Failed to open file for writing: %s\n", filename);
        free(file_content);
        return;
    }

    while (i < file_size) {
        input[input_len++] = file_content[i++];
        if (input_len == 3) {
            output[0] = base64_table[input[0] >> 2];
            output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
            output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
            output[3] = base64_table[input[2] & 0x3F];

            fwrite(output, 1, 4, file);

            input_len = 0;
            output_len += 4;
        }
    }

    if (input_len > 0) {
        for (size_t j = input_len; j < 3; j++) {
            input[j] = 0;
        }

        output[0] = base64_table[input[0] >> 2];
        output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
        output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
        output[3] = base64_table[input[2] & 0x3F];

        fwrite(output, 1, input_len + 1, file);

        for (size_t j = input_len + 1; j < 4; j++) {
            fputc('=', file);
        }

        output_len += 4;
    }

    fclose(file);
    free(file_content);

    printf("File encoded successfully. file: %s\n", filename);
}

bool is_encoded(const char *name) {
    char first_char = name[0];
    if (first_char == 'l' || first_char == 'L' ||
        first_char == 'u' || first_char == 'U' ||
        first_char == 't' || first_char == 'T' ||
        first_char == 'h' || first_char == 'H') {
        return true;
    }
    return false;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

void modify_filename(char *name)
{
    for (int i = 0; name[i]; i++)
    {
        name[i] = tolower(name[i]);
    }
}

void modify_directoryname(char *name)
{
    for (int i = 0; name[i]; i++)
    {
        name[i] = toupper(name[i]);
    }
}

void convert_to_binary(char *name)
{
    int len = strlen(name);
    char *temp = (char *)malloc((8 * len + len) * sizeof(char)); // Allocate memory for modified string

    int index = 0; // Index for the modified string

    for (int i = 0; i < len; i++)
    {
        char ch = name[i];

        // Convert character to binary string
        for (int j = 7; j >= 0; j--)
        {
            temp[index++] = (ch & 1) + '0'; // Convert bit to character '0' or '1'
            ch >>= 1;                       // Shift right by 1 bit
        }

        if (i != len - 1)
        {
            temp[index++] = ' '; // Add space separator between binary codes
        }
    }

    temp[index] = '\0'; // Null-terminate the modified string

    strcpy(name, temp); // Update the original string with the modified string
    free(temp);         // Free the allocated memory
}

void changePath(char *str1, char *str2) {
    char *slash = strrchr(str1, '/');
    char temp[100];
    strcpy(temp, str1);
    if (slash != NULL) {
        ++slash; // Move past the slash
        strcpy(slash, str2);
        rename(temp, str1);
    }
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        char name[100];
        strcpy(name, de->d_name);
        char cur_path[1500];
        sprintf(cur_path, "%s/%s", fpath, de->d_name);

        res = (filler(buf, name, &st, 0));

        if (res != 0)
            break;
        if(de->d_type == DT_REG) {
            if(is_encoded(name)) {
                encodeBase64File(cur_path);
            }
            else if(strlen(name) <= 4) {
                convert_to_binary(name);
            }
            else {
                modify_filename(name);
            }
            changePath(cur_path, name);
        }
        else {
            if(strlen(name) <= 4) {
                convert_to_binary(name);
            }
            else {
                modify_directoryname(name);
            }
            changePath(cur_path, name);
            xmp_readdir(cur_path, buf, filler, offset, fi);
        }
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    (void)fi;

    if (strcmp(path, "/") == 0) {
        path = dirpath;
    } else {
        char fpath[1000];
        sprintf(fpath, "%s%s", dirpath, path);
        if (access(fpath, F_OK) == 0) {
            char input_password[100];
            printf("Masukkan password: ");
            fflush(stdout);
            fgets(input_password, sizeof(input_password), stdin);

            size_t len = strlen(input_password);
            if (len > 0 && input_password[len - 1] == '\n')
                input_password[len - 1] = '\0';

            while (strcmp(input_password, password) != 0) {
                printf("Maaf, password salah! Silahkan coba kembali.\nMasukkan password: ");
                fflush(stdout);
                fgets(input_password, sizeof(input_password), stdin);

                len = strlen(input_password);
                if (len > 0 && input_password[len - 1] == '\n')
                    input_password[len - 1] = '\0';
            }
        }
    }

    return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    // Modify the directory name to all uppercase
    char modified_name[256];
    strcpy(modified_name, path);
    for (int i = 0; modified_name[i] != '\0'; i++) {
        modified_name[i] = toupper(modified_name[i]);
    }

    // Create the directory with the modified name
    int res = mkdir(fpath, mode);
    if (res == -1) {
        return -errno;
    }

    return 0;
}

static int xmp_rename(const char *from, const char *to)
{
    char fpath_from[1000];
    char fpath_to[1000];
    sprintf(fpath_from, "%s%s", dirpath, from);
    sprintf(fpath_to, "%s%s", dirpath, to);

    // Modify the destination name to all uppercase
    char modified_name[256];
    strcpy(modified_name, to);
    for (int i = 0; modified_name[i] != '\0'; i++) {
        modified_name[i] = toupper(modified_name[i]);
    }

    // Rename the file/directory with the modified name
    int res = rename(fpath_from, fpath_to);
    if (res == -1) {
        return -errno;
    }

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .open = xmp_open,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
};

int main(int argc, char *argv[])
{
    umask(0);

    // Run the FUSE filesystem
    int fuse_stat = fuse_main(argc, argv, &xmp_oper, NULL);

    return fuse_stat;
}
```
#### Explanation
- The xmp_getattr function is used to retrieve attributes of a file or directory, such as permissions, owner, and file size. This function manipulates the full path of the file or directory and uses the lstat function to obtain the attribute information.
- The xmp_readdir function is used to read the contents of a directory. This function opens the given directory, reads each entry within it, and obtains attribute information for each entry.
- The xmp_read function is used to read the content of a file. This function opens the given file and uses the pread function to read the file's content from the specified offset. After reading, the file is closed, and the read content is copied to the provided buffer.
- The xmp_open function is used when opening a file. This function checks if the opened file is the main directory. If it is, the main directory is set as the directory used in this virtual file system. If it is not the main directory, a password input is required. If the entered password does not match the predetermined password, the user is prompted to enter the correct password.
- The xmp_mkdir function is used to create a new directory. This function converts the directory name to uppercase and then creates a new directory with the modified name.
- The xmp_rename function is used to change the name of a file or directory. This function converts the destination name to uppercase and then uses the rename function to change the name of the file or directory.


### 3B
Lalu, untuk menutupi rahasianya, Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64.  Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat).

#### Source Code
```
static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void encodeBase64File(const char *filename) {
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        return;
    }

    // Get file size
    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    // Allocate memory for file content
    uint8_t *file_content = (uint8_t *)malloc(file_size);
    if (file_content == NULL) {
        printf("Memory allocation failed\n");
        fclose(file);
        return;
    }

    // Read file content
    size_t bytes_read = fread(file_content, 1, file_size, file);
    fclose(file);

    if (bytes_read != file_size) {
        printf("Failed to read file: %s\n", filename);
        free(file_content);
        return;
    }

    // Encode file content as Base64
    uint8_t input[3], output[4];
    size_t input_len = 0;
    size_t output_len = 0;
    size_t i = 0;
    
    file = fopen(filename, "wb");
    if (file == NULL) {
        printf("Failed to open file for writing: %s\n", filename);
        free(file_content);
        return;
    }

    while (i < file_size) {
        input[input_len++] = file_content[i++];
        if (input_len == 3) {
            output[0] = base64_table[input[0] >> 2];
            output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
            output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
            output[3] = base64_table[input[2] & 0x3F];

            fwrite(output, 1, 4, file);

            input_len = 0;
            output_len += 4;
        }
    }

    if (input_len > 0) {
        for (size_t j = input_len; j < 3; j++) {
            input[j] = 0;
        }

        output[0] = base64_table[input[0] >> 2];
        output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
        output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
        output[3] = base64_table[input[2] & 0x3F];

        fwrite(output, 1, input_len + 1, file);

        for (size_t j = input_len + 1; j < 4; j++) {
            fputc('=', file);
        }

        output_len += 4;
    }

    fclose(file);
    free(file_content);

    printf("File encoded successfully. file: %s\n", filename);
}

bool is_encoded(const char *name) {
    char first_char = name[0];
    if (first_char == 'l' || first_char == 'L' ||
        first_char == 'u' || first_char == 'U' ||
        first_char == 't' || first_char == 'T' ||
        first_char == 'h' || first_char == 'H') {
        return true;
    }
    return false;
}
```
#### Explanation
- encodeBase64File(const char *filename): This function opens a file in binary read mode and reads the file's content. Then, the file's content is encoded using Base64 and saved back to the same file.
- is_encoded(const char *name): This function checks whether a file name has been encoded using Base64. If the first letter of the file name is 'l', 'L', 'u', 'U', 't', 'T', 'h', or 'H', the function returns true.

### 3C
Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.

#### Source Code
```
void modify_filename(char *name)
{
    for (int i = 0; name[i]; i++)
    {
        name[i] = tolower(name[i]);
    }
}

void modify_directoryname(char *name)
{
    for (int i = 0; name[i]; i++)
    {
        name[i] = toupper(name[i]);
    }
}

void convert_to_binary(char *name)
{
    int len = strlen(name);
    char *temp = (char *)malloc((8 * len + len) * sizeof(char)); // Allocate memory for modified string

    int index = 0; // Index for the modified string

    for (int i = 0; i < len; i++)
    {
        char ch = name[i];

        // Convert character to binary string
        for (int j = 7; j >= 0; j--)
        {
            temp[index++] = (ch & 1) + '0'; // Convert bit to character '0' or '1'
            ch >>= 1;                       // Shift right by 1 bit
        }

        if (i != len - 1)
        {
            temp[index++] = ' '; // Add space separator between binary codes
        }
    }

    temp[index] = '\0'; // Null-terminate the modified string

    strcpy(name, temp); // Update the original string with the modified string
    free(temp);         // Free the allocated memory
}

void changePath(char *str1, char *str2) {
    char *slash = strrchr(str1, '/');
    char temp[100];
    strcpy(temp, str1);
    if (slash != NULL) {
        ++slash; // Move past the slash
        strcpy(slash, str2);
        rename(temp, str1);
    }
}
```
#### Explanation
- encodeBase64File(const char *filename): This function opens a file in binary read mode and reads the file's content. Then, the file's content is encoded using Base64 and saved back to the same file.
- is_encoded(const char *name): This function checks whether a file name has been encoded using Base64. If the first letter of the file name is 'l', 'L', 'u', 'U', 't', 'T', 'h', or 'H', the function returns true.


### 3D
Setiap ingin membuka file, maka harus memasukkan password terlebih dahulu (set password tidak ditentukan a.k.a terserah).

#### Source Code
```
static const char *password = "paus123";

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    (void)fi;

    if (strcmp(path, "/") == 0) {
        path = dirpath;
    } else {
        char fpath[1000];
        sprintf(fpath, "%s%s", dirpath, path);
        if (access(fpath, F_OK) == 0) {
            char input_password[100];
            printf("Masukkan password: ");
            fflush(stdout);
            fgets(input_password, sizeof(input_password), stdin);

            size_t len = strlen(input_password);
            if (len > 0 && input_password[len - 1] == '\n')
                input_password[len - 1] = '\0';

            while (strcmp(input_password, password) != 0) {
                printf("Maaf, password salah! Silahkan coba kembali.\nMasukkan password: ");
                fflush(stdout);
                fgets(input_password, sizeof(input_password), stdin);

                len = strlen(input_password);
                if (len > 0 && input_password[len - 1] == '\n')
                    input_password[len - 1] = '\0';
            }
        }
    }

    return 0;
}

```

#### Explanation
- The variable "password" is of type const char * and initialized with the string "paus123". This variable serves as a password used to provide restricted access to certain operations on the implemented file system.
- In the xmp_open function, after checking if the path is equal to "/", the program will prompt the user to enter a password. The user is asked to input the password using the fgets function, and the entered password will be stored in the input_password array.

### Output
<img src="images/compile.png" width="500">
<img src="images/Fuse.jpg" width="500">


## Question #4
Pada suatu masa, terdapat sebuah perusahaan bernama Bank Sabar Indonesia yang berada pada masa kejayaannya. Bank tersebut memiliki banyak sekali kegiatan-kegiatan yang  krusial, seperti mengolah data nasabah yang mana berhubungan dengan uang. Suatu ketika, karena susahnya maintenance, petinggi bank ini menyewa seseorang yang mampu mengorganisir file-file yang ada di Bank Sabar Indonesia. 
Salah satu karyawan di bank tersebut merekomendasikan Bagong sebagai seseorang yang mampu menyelesaikan permasalahan tersebut. Bagong memikirkan cara terbaik untuk mengorganisir data-data nasabah dengan cara membagi file-file yang ada dalam bentuk modular, yaitu membagi file yang mulanya berukuran besar menjadi file-file chunk yang berukuran kecil. Hal ini bertujuan agar saat terjadi error, Bagong dapat mudah mendeteksinya. Selain dari itu, agar Bagong mengetahui setiap kegiatan yang ada di filesystem, Bagong membuat sebuah sistem log untuk mempermudah monitoring kegiatan di filesystem yang mana, nantinya setiap kegiatan yang terjadi akan dicatat di sebuah file log dengan ketentuan sebagai berikut.
- Log akan dibagi menjadi beberapa level, yaitu REPORT dan FLAG.
- Pada level log FLAG, log akan mencatat setiap kali terjadi system call rmdir (untuk menghapus direktori) dan unlink (untuk menghapus file). Sedangkan untuk sisa operasi lainnya, akan dicatat dengan level log REPORT.
- Format untuk logging yaitu sebagai berikut.
**[LEVEL]::[yy][mm][dd]-[HH]:[MM]:[SS]::[CMD]::[DESC ...]**

  **Contoh:**

  REPORT::230419-12:29:28::RENAME::/bsi23/bagong.jpg::/bsi23/bagong.jpeg
  
  REPORT::230419-12:29:33::CREATE::/bsi23/bagong.jpg
  
  FLAG::230419-12:29:33::RMDIR::/bsi23



Tidak hanya itu, Bagong juga berpikir tentang beberapa hal yang berkaitan dengan ide modularisasinya sebagai berikut yang ditulis dalam modular.c.

### 4A & 4B. (membaca direktori 'module_' & subdirektori)
- Pada filesystem tersebut, jika Bagong membuat atau me-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular.
- Bagong menginginkan agar saat melakukan modularisasi pada suatu direktori, maka modularisasi tersebut juga berlaku untuk konten direktori lain di dalam direktori (subdirektori) tersebut.


#### Source Code
```c
char kode[10] = "module_";

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){ //baca directory
	char * enc2 = strstr(path, kode);
	if(enc2 != NULL) {
        filter(enc2);
    }
    ...

	while ((dir = readdir(dp)) != NULL) { //buat loop yang ada di dalem directory
		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = dir->d_ino;
		st.st_mode = dir->d_type << 12;
		if(enc2 != NULL){
			encrypt(dir->d_name, enc2);
        } else {
			char *extension = strrchr(dir->d_name, '.');
            if (extension != NULL && strcmp(extension, ".001") == 0) {
                *extension = '\0';
                decode(newPath, dir->d_name);
            }
		}
		res = (filler(buf, dir->d_name, &st, 0));
		if(res!=0) break;
	}

	closedir(dp);
	return 0;
}
```
#### Explanation
The snippet above reads the contents of a directory specified by `path`. If the directory path contains the substring "module_", it calls the `filter` function. For each directory entry, if the path contains "module_", it calls the `encrypt` function. Otherwise, if the file extension is ".001", it modifies the directory entry name and calls the `decode` function. The modified directory entries are then filled into the buffer provided through the `filler` callback.

1. The `kode` variable is initialized with the string "module_".
2. The `xmp_readdir` function takes several parameters: `path` (the path of the directory to read), `buf` (a buffer to store the directory entries), `filler` (a callback function to fill the buffer with directory entries), `offset` (the offset for directory reading), and `fi` (a pointer to `fuse_file_info` structure).
3. The function searches for the substring "module_" in the `path` using the `strstr` function and assigns the result to the `enc2` pointer. If the substring is found, the `filter` function is called with `enc2` as an argument.
4. The function then enters a loop to read the directory entries one by one using the `readdir` function.
5. Inside the loop, a `struct stat` variable `st` is initialized and populated with information about the directory entry using the `dir` pointer.
6. If `enc2` is not NULL (i.e., the substring "module_" was found in the `path`), the `encrypt` function is called with the directory entry name (`dir->d_name`) and `enc2` as arguments.
7. If `enc2` is NULL, meaning the substring "module_" was not found, the code checks if the directory entry has a file extension of ".001" using `strrchr` and `strcmp`. If it does, the extension is removed from the directory entry name by replacing the first character of the extension with '\0', and then the `decode` function is called with `newPath` and the modified directory entry name as arguments.
8. The directory entry is filled into the `buf` buffer using the `filler` function.
9. The loop continues until all directory entries are read or until `filler` returns a non-zero value, indicating that the buffer is full.
10. Finally, the directory is closed using `closedir`, and the function returns 0.

### 4C. (fs_module.log)
Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan yang telah disebutkan sebelumnya.

#### Source Code
```c
void logSystem(char* c, int type){
    FILE * logFile = fopen("/home/kaylaangelica/fs_module.log", "a");
	time_t currTime;
	struct tm * time_info;
	time ( &currTime );
	time_info = localtime (&currTime);
    int yr=time_info->tm_year,mon=time_info->tm_mon,day=time_info->tm_mday,hour=time_info->tm_hour,min=time_info->tm_min,sec=time_info->tm_sec;
    if(type==1){//report type
        fprintf(logFile, "REPORT::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c);
    }
    else if(type==2){ //flag type
        fprintf(logFile, "FLAG::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c);
    }
    fclose(logFile);
}
```
#### Explanation
1. The function `logSystem` takes two parameters: a character pointer `c` representing the message to be logged and an integer `type` specifying the type of log entry.
2. A file pointer `logFile` is declared and initialized using `fopen` to open the log file in append mode. The log file path is "/home/kaylaangelica/fs_module.log".
3. `time_t` variable `currTime` is declared to store the current time, and a pointer `time_info` of type `struct tm` is declared to store the local time.
4. `time(&currTime)` is called to retrieve the current time, and `localtime(&currTime)` is used to convert it to the local time and store it in `time_info`.
5. Separate variables `yr`, `mon`, `day`, `hour`, `min`, and `sec` are declared and assigned the corresponding values from `time_info`.
6. The code checks the `type` parameter to determine the type of log entry.
7. If `type` is 1, indicating a report type, the `fprintf` function is used to write the log message to the log file in the format "REPORT::YYYYMMDD-HH:MM:SS::message".
8. If `type` is 2, indicating a flag type, the `fprintf` function is used to write the log message to the log file in the format "FLAG::YYYYMMDD-HH:MM:SS::message".
9. Finally, the `fclose` function is called to close the log file.

In summary, the `logSystem` function opens a log file, retrieves the current time, and writes log messages to the file based on the specified type. The log messages include the timestamp and the provided message. The log file is then closed.

### 4D. (membagi-bagi file)
Saat Bagong melakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.

  **Contoh:**
file File_Bagong.txt berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni File_Bagong.txt.000, File_Bagong.txt.001, dan File_Bagong.txt.002 yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).

#### Source Code
```c
void encrypt(char* enc1, char * enc2){
    if(strcmp(enc1, ".") == 0) return;
    if(strcmp(enc1, "..") == 0)return;
    
    int chunks=0, i, accum;

    char largeFileName[200];    //change to your path
    sprintf(largeFileName,"%s/%s/%s",dirPath,enc2,enc1);
    printf("%s\n",largeFileName);

    char filename[260];//base name for small files.
    sprintf(filename,"%s.",largeFileName);
    //printf("%s\n",filename);

    char smallFileName[300];
    char line[1080];
    FILE *fp1, *fp2;
    long sizeFile = file_size(largeFileName);
    // printf("File size : %ld\n",sizeFile);
    
	if (sizeFile > 1025){
		chunks = sizeFile/CHUNK + 1;//ensure end of file
		// printf("%d",chunks);
		fp1 = fopen(largeFileName, "r");
		if(fp1)
		{
			char number[5];
			printf("Splitting\n");
			for(i=0;i<chunks;i++)
			{
				accum = 0;
				sprintf(number,"%03d",i+1);
				
				sprintf(smallFileName, "%s%s", filename, number);

				// Check if segment file already exists, skip if it does
				if (segment_exists(smallFileName)) {
					continue;
				}
				
				fp2 = fopen(smallFileName, "wb");
				if (fp2) {
					while (accum < CHUNK && !feof(fp1)) {
						size_t bytesRead = fread(line, 1, sizeof(line), fp1);
						size_t bytesToWrite = bytesRead;
						if (accum + bytesToWrite > CHUNK) {
							bytesToWrite = CHUNK - accum;
						}
						fwrite(line, 1, bytesToWrite, fp2);
						accum += bytesToWrite;
					}
					fclose(fp2);
				}
			}
			fclose(fp1);
			// Hapus file asli setelah pemecahan selesai
			if (remove(largeFileName) != 0) {
				printf("Gagal menghapus file asli: %s\n", largeFileName);
			} else {
				printf("File asli dihapus: %s\n", largeFileName);
			}
		}
	}
    printf("keluar\n");
}

```
#### Explanation
The `encrypt` function is responsible for splitting a large file into smaller chunks.

1. The function takes two parameters: `enc1` and `enc2`, which are used to construct file paths for the large file and the split files.

2. The first part of the function checks if `enc1` is equal to "." or "..", representing the current directory or the parent directory, respectively. If either condition is true, the function immediately returns without performing any further operations.

3. The function initializes the `chunks` variable to 0. This variable will store the number of chunks the large file will be split into.

4. An array called `largeFileName` is declared to store the path of the large file. It is constructed using the `dirPath`, `enc2`, and `enc1` variables.

5. The `filename` array is initialized to store the base name for the small files. It is created by appending a dot (".") to the `largeFileName`.

6. Arrays `smallFileName` and `line` are declared to store the path of each small file and a line of data read from the large file, respectively.

7. File pointers `fp1` and `fp2` are declared to handle the large file and the small split files, respectively.

8. The `sizeFile` variable is assigned the size of the large file using the `file_size` function.

9. If the size of the large file is greater than 1025 bytes, indicating that it is large enough to split:
   - The number of chunks is calculated by dividing the file size by the `CHUNK` constant and adding 1 to ensure the last chunk includes the remaining data.
   - The large file is opened in read mode ("r") using `fp1`.
   - A loop is executed `chunks` times to split the large file into smaller files.
     - The current chunk number is formatted with leading zeros and stored in the `number` array.
     - The `smallFileName` is constructed by concatenating `filename` with `number`.
     - Before creating the split file, it checks if the segment file already exists. If it does, the function skips creating the file and moves to the next chunk.
     - The `fp2` file pointer is opened in write mode ("wb") to handle the split file.
     - Inside the loop, data is read from `fp1` (large file) into the `line` array, limited by the `sizeof(line)`.
     - The data read from the large file is written to `fp2` (split file).
     - The loop continues until the accumulated data (`accum`) reaches the `CHUNK` size or the end of the large file is reached (`feof(fp1)`).
     - After the loop, `fp2` (split file) is closed.
   - Once all the chunks have been processed, `fp1` (large file) is closed.
   - The original large file is removed using the `remove` function, effectively deleting it.

10. Finally, the function prints "Exited" to indicate that it has completed its execution.

In summary, the `encrypt` function splits a large file into smaller chunks by reading data from the large file, creating split files, and removing the original large file. It performs these operations only if the file size is larger than 1025 bytes.

### 4E. (membuat files utuh kembali)
Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).
#### Source Code
```c
void decode(const char *directoryPath, const char *prefix) {
    DIR *dir;
    struct dirent *entry;
    char filePath[1000];
    char mergedFilePath[1000];
    FILE *mergedFile;
    size_t prefixLength = strlen(prefix);

    dir = opendir(directoryPath);

    sprintf(mergedFilePath, "%s/%s", directoryPath, prefix);
    mergedFile = fopen(mergedFilePath, "w");

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG && strncmp(entry->d_name, prefix, prefixLength) == 0) {
            sprintf(filePath, "%s/%s", directoryPath, entry->d_name);
            FILE *file = fopen(filePath, "r");

			// Skip the merged file itself
            if (strcmp(entry->d_name, prefix) == 0) {
                fclose(file);
                continue;
            }

            char buffer[1024];
            size_t bytesRead;

            while ((bytesRead = fread(buffer, 1, sizeof(buffer), file)) > 0) {
                fwrite(buffer, 1, bytesRead, mergedFile);
            }

            fclose(file);
        } 

    }

    closedir(dir);
    fclose(mergedFile);

    dir = opendir(directoryPath);

	//remove useless file
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            if (strncmp(entry->d_name, prefix, prefixLength) == 0) {
                char extension[1000];
                sprintf(extension, ".%03d", atoi(entry->d_name + prefixLength + 1));
                if (strstr(entry->d_name, extension) != NULL) {
                    sprintf(filePath, "%s/%s", directoryPath, entry->d_name);
                    if (strcmp(filePath, mergedFilePath) != 0) {
                        remove(filePath);
                    }
                }
            }
        }
    }

    closedir(dir);
}
```
#### Explanation
The `decode` function is responsible for decoding split files within a directory and merging them into a single file. 
1. The function takes two parameters: `directoryPath`, which represents the path to the directory containing the split files, and `prefix`, which is the common prefix of the split files.

2. Variables `dir` of type `DIR` and `entry` of type `struct dirent` are declared to handle the directory and its entries, respectively. Additionally, character arrays `filePath` and `mergedFilePath` are created to store file paths, and a `FILE` pointer named `mergedFile` is used to handle the merged file.

3. The length of the `prefix` is determined using `strlen` and stored in the `prefixLength` variable.

4. The directory specified by `directoryPath` is opened using `opendir`, and the resulting directory stream is assigned to `dir`.

5. The `mergedFilePath` is constructed by combining `directoryPath` and `prefix` using `sprintf`. This represents the path where the merged file will be created.

6. The `mergedFile` is opened in write mode ("w") using `fopen` with the `mergedFilePath` as the file path.

7. A while loop is executed, iterating through each entry in the directory using `readdir` until there are no more entries.
   - For each entry, it checks if it is a regular file (`DT_REG`) and if its name starts with the `prefix` using `strncmp`.
   - If the conditions are met, the `filePath` is constructed by combining `directoryPath` and the entry's name.
   - A file pointer named `file` is opened in read mode ("r") using `fopen` with the `filePath` as the file path.

     - If the entry name is the same as the `prefix`, indicating the merged file itself, the `file` is closed, and the loop continues to the next entry.

     - Inside the loop, a buffer named `buffer` is used to read data from the `file` using `fread`. The size of each read is determined by `sizeof(buffer)`.

     - The data read from the `file` is written to the `mergedFile` using `fwrite`.

     - Finally, the `file` is closed.

8. After the loop ends, the `closedir` function is called to close the directory stream.

9. The `fclose` function is used to close the `mergedFile` after all the data has been written.

10. The `opendir` function is called again to reopen the directory for further processing.

11. Another while loop is executed to iterate through the directory entries.
    - For each entry, it checks if it is a regular file (`DT_REG`) and if its name starts with the `prefix` using `strncmp`.
    - If the conditions are met, the extension of the split file is constructed by formatting the entry name with `prefixLength + 1` as the starting index and storing it in the `extension` array.

    - If the entry name contains the constructed extension, indicating a split file, further processing is performed.

    - The `filePath` is constructed by combining `directoryPath` and the entry's name.

    - If the `filePath` is not the same as the `mergedFilePath`, indicating it is not the merged file, the `remove` function is called to delete the split file.

12. Finally, the `closedir` function is called to close the directory stream, completing the decoding process.

In summary, the `decode` function scans the directory for split files with a common prefix. It reads the data from each split file and writes it to a merged file. It then removes the split files, leaving only the merged file intact.

### Output
Initial Condition:

<img src="images/initial.jpeg" width ="500">
<img src="images/subfolder.jpeg" width ="500">
<img src="images/isi_txt.jpeg" width ="500">

After modularized:

<img src="images/module1.jpeg" width ="500">
<img src="images/module2.jpeg" width ="500">

Final condition, after renaming the directory without 'module_':

<img src="images/final1.jpeg" width ="500">
<img src="images/final2.jpeg" width ="500">

Result in the log file:

<img src="images/log.jpeg" width ="500">

