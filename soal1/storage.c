#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main() {
    int status;
    pid_t child_id;

    child_id = fork();

    if (child_id < 0) {
        printf("Failed to create a new process.\n");
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        // This is the child process
        char *downloadCommand[] = {"kaggle", "datasets", "download", "-d", "bryanb/fifa-player-stats-database", NULL};
        execvp("kaggle", downloadCommand);
        exit(EXIT_FAILURE);
    }

    waitpid(child_id, &status, 0);

    if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
        child_id = fork();

        if (child_id < 0) {
            printf("Failed to create a new process.\n");
            exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
            // This is the child process
            char *extractCommand[] = {"unzip", "fifa-player-stats-database.zip", NULL};
            execvp("unzip", extractCommand);
            exit(EXIT_FAILURE);
        }

        waitpid(child_id, &status, 0);

        system("awk -F\",\" '{if ($3 < 25 && $8 > 85 && $9 != \"Manchester City\") print \"Name :\",$2,\"\\n Club: \",$9,\"\\n Age: \",$3,\"\\n Potential: \",$8,\"\\n Photo: \",$4,\"\\n Nationality: \",$5,\"\\n\"}' FIFA23_official_data.csv");
    }

    return 0;
}

