#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/wait.h>
#define CHUNK 1024 //approximate target size of small file

static  const  char * dirPath = "/home/kaylaangelica";

char kode[10] = "module_";

void logSystem(char* c, int type){
    FILE * logFile = fopen("/home/kaylaangelica/fs_module.log", "a");
	time_t currTime;
	struct tm * time_info;
	time ( &currTime );
	time_info = localtime (&currTime);
    int yr=time_info->tm_year,mon=time_info->tm_mon,day=time_info->tm_mday,hour=time_info->tm_hour,min=time_info->tm_min,sec=time_info->tm_sec;
    if(type==1){//report type
        fprintf(logFile, "REPORT::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c);
    }
    else if(type==2){ //flag type
        fprintf(logFile, "FLAG::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c);
    }
    fclose(logFile);
}

int segment_exists(const char* filename) {
    FILE* file = fopen(filename, "rb");
    if (file == NULL) {
        return 0;  // File does not exist
    }
    fclose(file);
    return 1;  // File exists
}

long file_size(char *name){
    FILE *fp = fopen(name, "rb"); //must be binary read to get bytes
    // printf("%s\n",name);
    long size=-1;
    if(fp)
    {
        fseek (fp, 0, SEEK_END);
        size = ftell(fp)+1;
        fclose(fp);
    }
    return size;
}

void decode(const char *directoryPath, const char *prefix) {
    DIR *dir;
    struct dirent *entry;
    char filePath[1000];
    char mergedFilePath[1000];
    FILE *mergedFile;
    size_t prefixLength = strlen(prefix);

    dir = opendir(directoryPath);

    sprintf(mergedFilePath, "%s/%s", directoryPath, prefix);
    mergedFile = fopen(mergedFilePath, "w");

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG && strncmp(entry->d_name, prefix, prefixLength) == 0) {
            sprintf(filePath, "%s/%s", directoryPath, entry->d_name);
            FILE *file = fopen(filePath, "r");

			// Skip the merged file itself
            if (strcmp(entry->d_name, prefix) == 0) {
                fclose(file);
                continue;
            }

            char buffer[1024];
            size_t bytesRead;

            while ((bytesRead = fread(buffer, 1, sizeof(buffer), file)) > 0) {
                fwrite(buffer, 1, bytesRead, mergedFile);
            }

            fclose(file);
        } 

    }

    closedir(dir);
    fclose(mergedFile);

    dir = opendir(directoryPath);

	//remove useless file
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            if (strncmp(entry->d_name, prefix, prefixLength) == 0) {
                char extension[1000];
                sprintf(extension, ".%03d", atoi(entry->d_name + prefixLength + 1));
                if (strstr(entry->d_name, extension) != NULL) {
                    sprintf(filePath, "%s/%s", directoryPath, entry->d_name);
                    if (strcmp(filePath, mergedFilePath) != 0) {
                        remove(filePath);
                    }
                }
            }
        }
    }

    closedir(dir);
}

void encrypt(char* enc1, char * enc2){
    if(strcmp(enc1, ".") == 0) return;
    if(strcmp(enc1, "..") == 0)return;
    
    int chunks=0, i, accum;

    char largeFileName[200];    //change to your path
    sprintf(largeFileName,"%s/%s/%s",dirPath,enc2,enc1);
    printf("%s\n",largeFileName);

    char filename[260];//base name for small files.
    sprintf(filename,"%s.",largeFileName);
    //printf("%s\n",filename);

    char smallFileName[300];
    char line[1080];
    FILE *fp1, *fp2;
    long sizeFile = file_size(largeFileName);
    // printf("File size : %ld\n",sizeFile);
    
	if (sizeFile > 1025){
		chunks = sizeFile/CHUNK + 1;//ensure end of file
		// printf("%d",chunks);
		fp1 = fopen(largeFileName, "r");
		if(fp1)
		{
			char number[5];
			printf("Splitting\n");
			for(i=0;i<chunks;i++)
			{
				accum = 0;
				sprintf(number,"%03d",i+1);
				
				sprintf(smallFileName, "%s%s", filename, number);

				// Check if segment file already exists, skip if it does
				if (segment_exists(smallFileName)) {
					continue;
				}
				
				fp2 = fopen(smallFileName, "wb");
				if (fp2) {
					while (accum < CHUNK && !feof(fp1)) {
						size_t bytesRead = fread(line, 1, sizeof(line), fp1);
						size_t bytesToWrite = bytesRead;
						if (accum + bytesToWrite > CHUNK) {
							bytesToWrite = CHUNK - accum;
						}
						fwrite(line, 1, bytesToWrite, fp2);
						accum += bytesToWrite;
					}
					fclose(fp2);
				}
			}
			fclose(fp1);
			// Hapus file asli setelah pemecahan selesai
			if (remove(largeFileName) != 0) {
				printf("Gagal menghapus file asli: %s\n", largeFileName);
			} else {
				printf("File asli dihapus: %s\n", largeFileName);
			}
		}
	}
    printf("keluar\n");
}

void filter(char * enc1){
    if(strcmp(enc1, ".") == 0) return;
    if(strcmp(enc1, "..") == 0)return;
	if(strstr(enc1, "/") == NULL)return;
}

static  int  xmp_getattr(const char *path, struct stat *stbuf){
	char * enc2 = strstr(path, kode);
	if(enc2 != NULL) {
        filter(enc2);
    }
	char newPath[1000];
	int res;
	sprintf(newPath,"%s%s", dirPath, path);
	res = lstat(newPath, stbuf);
	if (res == -1)
		return -errno;
	return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){ //baca directory
	char * enc2 = strstr(path, kode);
	if(enc2 != NULL) {
        filter(enc2);
    }

	char newPath[1000];
	if(strcmp(path,"/") == 0){
		path=dirPath;
		sprintf(newPath,"%s",path);
	} else sprintf(newPath, "%s%s",dirPath,path);

	int res = 0;
	struct dirent *dir;
	DIR *dp;
	(void) fi;
	(void) offset;
	dp = opendir(newPath);
	if (dp == NULL) return -errno;

	while ((dir = readdir(dp)) != NULL) { //buat loop yang ada di dalem directory
		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = dir->d_ino;
		st.st_mode = dir->d_type << 12;
		if(enc2 != NULL){
			encrypt(dir->d_name, enc2);
        } else {
			char *extension = strrchr(dir->d_name, '.');
            if (extension != NULL && strcmp(extension, ".001") == 0) {
                *extension = '\0';
                decode(newPath, dir->d_name);
            }
		}
		res = (filler(buf, dir->d_name, &st, 0));
		if(res!=0) break;
	}

	closedir(dp);
	return 0;
}

static int xmp_mkdir(const char *path, mode_t mode){ //buat bikin directory baru

	char newPath[1000];
	if(strcmp(path,"/") == 0){
		path=dirPath;
		sprintf(newPath,"%s",path);
	}
	else sprintf(newPath, "%s%s",dirPath,path);

	int res = mkdir(newPath, mode);
    char str[100];
	sprintf(str, "MKDIR::%s", path);
	logSystem(str,1);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev){//buat file

	char newPath[1000];
	if(strcmp(path,"/") == 0){
		path = dirPath;
		sprintf(newPath,"%s",path);
	} else sprintf(newPath, "%s%s",dirPath,path);
	int res;

	if (S_ISREG(mode)) {
		res = open(newPath, O_CREAT | O_EXCL | O_WRONLY, mode);
		if (res >= 0)
			res = close(res);
	} else if (S_ISFIFO(mode))
		res = mkfifo(newPath, mode);
	else
		res = mknod(newPath, mode, rdev);
    char str[100];
	sprintf(str, "CREATE::%s", path);
	logSystem(str,1);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_unlink(const char *path) { //ngehapus file
	printf("unlink\n");
	char * enc2 = strstr(path, kode);
	if(enc2 != NULL) {
        filter(enc2);
    }

	char newPath[1000];
	if(strcmp(path,"/") == 0){
		path=dirPath;
		sprintf(newPath,"%s",path);
	} else sprintf(newPath, "%s%s",dirPath,path);
    char str[100];
	sprintf(str, "REMOVE::%s", path);
	logSystem(str,2);
	int res;
	res = unlink(newPath);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_rmdir(const char *path) {//ngehapus directory
	printf("rmdir\n");
	char * enc2 = strstr(path, kode);
	if(enc2 != NULL) {
        filter(enc2);
    }

	char newPath[1000];
	sprintf(newPath, "%s%s",dirPath,path);
    char str[100];
	sprintf(str, "RMDIR::%s", path);
	logSystem(str,2);
	int res;
	res = rmdir(newPath);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_rename(const char *from, const char *to) { //buat renme
	printf("\nRENAME!!!\n");
	char fileFrom[1000],fileTo[1000];
	sprintf(fileFrom,"%s%s",dirPath,from);
	sprintf(fileTo,"%s%s",dirPath,to);

    char str[100];
	sprintf(str, "RENAME::%s::%s", from, to);
	logSystem(str,1);
	int res;
	res = rename(fileFrom, fileTo);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi){ //open file
	char newPath[1000];
	sprintf(newPath, "%s%s",dirPath,path);
	int res;
	res = open(newPath, fi->flags);
	if (res == -1)
		return -errno;
	close(res);
	return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){ //read file
	
	char newPath[1000];
	sprintf(newPath, "%s%s",dirPath,path);
	int fd;
	int res;

	(void) fi;
	fd = open(newPath, O_RDONLY);
	if (fd == -1)
		return -errno;
	res = pread(fd, buf, size, offset);
	if (res == -1)
		res = -errno;
	close(fd);
	return res;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) { //write file
	
	char newPath[1000];
	sprintf(newPath, "%s%s", dirPath, path);
	
    int fd;
	int res;
	(void) fi;
	fd = open(newPath, O_WRONLY);
	if (fd == -1)
		return -errno;
    char str[100];
	sprintf(str, "WRITE::%s", path);
	logSystem(str,1);
	res = pwrite(fd, buf, size, offset);
	if (res == -1)
		res = -errno;
	close(fd);
	return res;
}

static struct fuse_operations xmp_oper = {

	.getattr = xmp_getattr,
	.readdir = xmp_readdir,
	.read = xmp_read,
	.mkdir = xmp_mkdir,
	.mknod = xmp_mknod,
	.unlink = xmp_unlink,
	.rmdir = xmp_rmdir,
	.rename = xmp_rename,
	.open = xmp_open,
	.write = xmp_write,

};

int  main(int  argc, char *argv[]){
	umask(0);

	return fuse_main(argc, argv, &xmp_oper, NULL);
}